package com.vytrykhovskyi;

public class Main {
    public static void main(String[] args)  {
        MyClass person = new MyClass("Andrii", "Vytrykhovskyi", 23, 66.6);
        System.out.println(person);

        Analyser.printAnnotationsAndFieldsWithIt(person);

        System.out.println("\nInvoke methods: ");
        Analyser.invokeMethods(person);

        Analyser.set(person, "weight", 70.0);
        System.out.println("\n" + person);

        HelperClass.helperClass(person);
    }
}
