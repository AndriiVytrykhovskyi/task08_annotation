package com.vytrykhovskyi;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

class Analyser {
    static void printAnnotationsAndFieldsWithIt(Object o) {
        Class<?> clz = o.getClass();
        System.out.println("\nFields with my custom annotation:");
        for (Field field : clz.getDeclaredFields()) {
            if (field.isAnnotationPresent(MyAnnotation.class)) {
                System.out.println(field.getName() + " " + Arrays.toString(field.getDeclaredAnnotations()));
            }
        }
    }

    static void set(Object o, String fieldname, double fieldValue) {
        try {
            Class<?> c = o.getClass();
            Field w = c.getDeclaredField(fieldname);
            w.setAccessible(true);
            w.setDouble(o, fieldValue);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    static void invokeMethods(Object o) {
        Class<?> c = o.getClass();
        try {
            Class[] paramTypes1 = new Class[]{int.class, double.class};
            Method m1 = c.getDeclaredMethod("sumMethod", paramTypes1);
            Object[] args1 = new Object[]{5, 10.1};
            Double d = (Double) m1.invoke(o, args1);
            System.out.println(d);

            Class[] paramTypes2 = new Class[]{int.class, int.class};
            Method m2 = c.getDeclaredMethod("multiplyMethod", paramTypes2);
            m2.setAccessible(true);
            Object[] args2 = new Object[]{5, 20};
            Integer i = (Integer) m2.invoke(o, args2);
            System.out.println(i);

            Class[] paramTypes3 = new Class[]{String.class, String.class};
            Method m3 = c.getDeclaredMethod("writeString", paramTypes3);
            Object[] args3 = new Object[]{"I", "You"};
            String str = (String) m3.invoke(o, args3);
            System.out.println(str);

            int[] numbers = new int[]{1, 2, 4, 10};
            Class[] paramTypes4 = new Class[]{String.class, int[].class};
            Method m4 = c.getDeclaredMethod("myMethod", paramTypes4);
            Object[] args4 = new Object[]{"First with string and numbers array", numbers};
            m4.invoke(o, args4);

            String[] strings = new String[]{"Second", "with", "strings", "array"};
            Class[] paramTypes5 = new Class[]{String[].class};
            Method m5 = c.getDeclaredMethod("myMethod", paramTypes5);
            Object[] args5 = new Object[]{strings};
            m5.invoke(o, args5);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}

