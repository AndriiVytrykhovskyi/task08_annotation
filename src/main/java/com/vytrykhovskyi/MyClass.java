package com.vytrykhovskyi;

public class MyClass {
    @MyAnnotation(str = "Name", value = 24)
    private String name;
    private String surname;
    @MyAnnotation(str = "Age", value = 100)
    private int age;
    private double weight;

    MyClass(String name, String surname, int age, double weight) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.weight = weight;
    }

    public double sumMethod(int a, double b) {
        return a + b;
    }

    private int multiplyMethod(int a, int b) {
        return a * b;
    }

    protected String writeString(String a, String b) {
        return a + " and " + b;
    }

    public void myMethod(String str, int... args) {
        System.out.println(str);
        for (int a : args) {
            System.out.print(a + " ");
        }
        System.out.println();
    }

    public void myMethod(String... args) {
        for (String str : args) {
            System.out.print(str + " ");
        }
    }

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", weight=" + weight +
                '}';
    }
}
